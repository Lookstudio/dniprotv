const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SlivSchema = new Schema({

    title:{
        type: String,
        required: true
    },
    data:{
        type: Date,
        required: true
    },
    oper:{
        type: String,
        required: true
    },
    jur:{
        type: String,
        required: true
    },

});

module.exports = mongoose.model('sliv', SlivSchema);