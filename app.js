const express = require('express');
const app =  express();
const path = require('path');
const exphbs = require('express-handlebars');
const bodyParser = require('body-parser');
const mysql = require('mysql');

//mongodb connection
/*
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/dniprotv', {useMongoClient: true}).then(db =>{
    console.log('Mongo connect');
}).catch(error=>console.log("Not Connect" + error));
*/

//mysql connection

const db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'dnipro_tv'
});

db.connect((err) =>{
    if(err){
        throw err;
    }
    console.log('Mysql Connected..');
});

app.get('/test/:id', (req, res) =>{
    let sql = 'SELECT * FROM Montageri WHERE id = ' +req.params.id;
    let query = db.query(sql, (err, results[] )=>{
        if(err) throw err;
            console.log(results);
           res.send(results[name]);
    });
});


app.use(express.static(path.join(__dirname, 'public')));


app.engine('handlebars', exphbs({defaultLayout: 'home'}));
app.set('view engine', 'handlebars');


app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

//загрузка путей

const home = require('./routes/home/index');
const admin = require('./routes/admin/index');
const sliv = require('./routes/admin/sliv');

//исопльзование путей
app.use('/', home);
app.use('/admin', admin);
app.use('/admin/sliv', sliv);


app.listen(4500, ()=>{

    console.log('listing on port 4500')

});
