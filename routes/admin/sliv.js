const express = require('express');
const router = express.Router();
const SlivModel = require('../../models/SlivModel');

router.all('/*', (req, res, next)=>{

    req.app.locals.layout = 'admin';
    next();
});

router.get('/upload', (req, res)=>{
    res.render('admin/sliv/upload');
});

router.post('/upload', (req, res)=>{


    const newSliv = new Sliv({
       title: req.body.title,
        data: req.body.data,
        oper: req.body.oper,
        jur: req.body.jur,


    });

    newSliv.save().then(savedPost =>{

        res.redirect('/admin/sliv');

    }).catch(error =>{
        console.log('could not save');
    });

    console.log(req.body);
});

module.exports = router;