const express = require('express');
const router = express.Router();


router.all('/*', (req, res, next)=>{

    req.app.locals.layout = 'home';
    next();
});

router.get('/', (req, res)=>{
    res.render('home/index');
});

router.get('/jurnalisti', (req, res)=>{
    res.render('home/jurnalisti');
});

router.get('/operatori', (req, res)=>{
    res.render('home/operatori');
});

router.get('/montageri', (req, res)=>{
    res.render('home/montageri');
});

router.get('/sliv', (req, res)=>{
    res.render('/sliv');
});

router.get('/sborka', (req, res)=>{
    res.render('action/sborka');
});

module.exports = router;